'use strict';

/**
 * @ngdoc function
 * @name guiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the guiApp
 */
angular.module('guiApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
