'use strict';

/**
 * @ngdoc function
 * @name guiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the guiApp
 */
angular.module('guiApp')
  .controller('MainCtrl', function ($scope, exampleService) {

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    exampleService.exampleGet();
  });
