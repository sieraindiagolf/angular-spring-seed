'use strict';

/**
 * @ngdoc service
 * @name guiApp.exampleService
 * @description
 * # exampleService
 * Service in the guiApp.
 */
angular.module('guiApp')
  .service('exampleService', function exampleService($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    function exampleGet () {
        $http.get('/data');
    }

    var service = {
        exampleGet: exampleGet
    };

    return service;
  });
