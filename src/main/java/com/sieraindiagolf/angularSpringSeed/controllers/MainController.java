package com.sieraindiagolf.angularSpringSeed.controllers;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stefan on 5/23/2015.
 */

@Controller
public class MainController {

    @RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index.html");
    }

    @RequestMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HashMap> getList () {
        List<HashMap> someList = new ArrayList<HashMap>();

        HashMap aModel = new HashMap();
        aModel.put("key1", 1);
        aModel.put("key2", 2);
        someList.add(aModel);
        someList.add(aModel);
        someList.add(aModel);
        someList.add(aModel);
        someList.add(aModel);

        return someList;
    }
}
